export interface GeneratedExcelResponse {
    status: number;
    message?: string | unknown;
    fileName?: string
}
