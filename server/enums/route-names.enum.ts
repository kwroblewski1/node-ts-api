export enum RouteName {
    FILMS = '/films',
    FAVORITES = '/favorites',
    FAVORITES_BY_ID = '/favorites/:id',
    FAVORITES_EXCEL_BY_ID = '/favorites/:id/file'
}
